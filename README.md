[![pipeline status](https://gitlab.com/nicolalandro/flutter_test_app/badges/master/pipeline.svg)](https://gitlab.com/nicolalandro/flutter_test_app/-/commits/master)
[![coverage report](https://gitlab.com/nicolalandro/flutter_test_app/badges/master/coverage.svg)](https://gitlab.com/nicolalandro/flutter_test_app/-/commits/master)


# flutter_test_app
This code contains a flutter simple app with all kind of test, integrated correctly with gitlab

## run test
* run all tests with coverage
```
flutter test --coverage
```

* run single test
```
flutter test --plain-name="simple test name"
```

# run ui test
```
# terminal 1
cd test_driver
./chromedriver --port=4444

# terminal 2
flutter drive --target=test_driver/app.dart --release
```

## gitlab badges for ci
* at url https://gitlab.com/nicolalandro/flutter_test_app/-/settings/ci_cd
* set re for coverage
```
\s*lines\.*:\s*([\d\.]+%)
```

## Run drive test with docker
```
cd docker
docker build -t flutter_test_drive .

cd ..
docker-compose up
```

## Release docker container
```
cd docekr
docker login registry.gitlab.com
./release_container.sh
```